import React from "react";
import { NavLink } from "react-router-dom";
import "./Sidebar.scss";

const Sidebar = () => {
  return (
    <ul className="sidebar">
      <li>
        <NavLink
          className="sidebar-item"
          activeClassName="is-active"
          to="/shop"
        >
          Shop
        </NavLink>
      </li>
      <li>
        <NavLink
          className="sidebar-item"
          activeClassName="is-active"
          to="/favorites"
        >
          Favorites
        </NavLink>
      </li>
      <li>
        <NavLink
          className="sidebar-item"
          activeClassName="is-active"
          to="/cart"
        >
          Cart
        </NavLink>
      </li>
    </ul>
  );
};

export default Sidebar;

import React from "react";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";
import "./CartForm.scss";
import CartInput from "../CartInput/CartInput";
import { useDispatch } from "react-redux";
import { deleteCart, getCart } from "../../store/operations";
import { SET_USER_CART } from "../../store/types";

const validationFormsSchema = Yup.object().shape({
  userName: Yup.string().required("Required name"),
  userSurname: Yup.string().required("Required surname"),
  userAge: Yup.number().required("Required age"),
  orderAddress: Yup.string().required("Required order address"),
  userPhoneNumber: Yup.number().required("Required phone number"),
});

const CartForm = () => {
  const dispatch = useDispatch();

  const setUserCart = (userInfo, cart) => {
    dispatch({
      type: SET_USER_CART,
      payload: { userInfo: userInfo, cart: cart },
    });
  };

  const submitForm = (
    { userName, userSurname, userAge, orderAddress, userPhoneNumber },
    { resetForm }
  ) => {
    const userInfo = {
      userName: userName,
      userSurname: userSurname,
      userAge: userAge,
      orderAddress: orderAddress,
      userPhoneNumber: userPhoneNumber,
    };
    const cart = getCart();

    setUserCart(userInfo, cart);
    deleteCart();
    resetForm({});

    console.log("Cart:", cart);
    console.log("User info:", userInfo);
  };

  return (
    <div className="cartform-container">
      <h2>Enter order data</h2>
      <Formik
        initialValues={{
          userName: "",
          userSurname: "",
          userAge: "",
          orderAddress: "",
          userPhoneNumber: "",
        }}
        onSubmit={submitForm}
        validationSchema={validationFormsSchema}
        validateOnChange={false}
        validateOnBlur={false}
      >
        {(formikProps) => {
          // console.log(formikProps);
          return (
            <div className="cartform-form">
              <Form>
                <Field
                  component={CartInput}
                  name="userName"
                  type="text"
                  label="Name"
                />
                <Field
                  component={CartInput}
                  name="userSurname"
                  type="text"
                  label="Surname"
                />
                <Field
                  component={CartInput}
                  name="userAge"
                  type="number"
                  label="Age"
                />
                <Field
                  component={CartInput}
                  name="orderAddress"
                  type="text"
                  label="Order address"
                />
                <Field
                  component={CartInput}
                  name="userPhoneNumber"
                  type="number"
                  label="Phone number"
                />
                <button type="submit">Checkout</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default CartForm;

import React from "react";
import Card from "../Card/Card";
import "./CardList.scss";
import { useSelector } from "react-redux";

const CardList = () => {
  const products = useSelector((state) => state.products.data);

  return (
    <div className="container">
      {products.map((product) => {
        return <Card key={product.articul} product={product} toCart />;
      })}
    </div>
  );
};

export default CardList;

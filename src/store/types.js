export const LOAD_CARDS_SUCCESS = "LOAD_CARDS_SUCCESS";
export const TOGGLE_FAVORITE = "TOGGLE_FAVORITE";
export const SHOW_MODAL = "SHOW_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const SET_USER_CART = "SET_USER_CART";
